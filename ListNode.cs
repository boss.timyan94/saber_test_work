﻿using System;

namespace Saber_Double_Nodes_Serialization
{
    class ListNode
    {
        public string Data;

        public ListNode Prev;
        public ListNode Next;
        public ListNode Rand;
    }
}
