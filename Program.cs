﻿using System;
using System.IO;

namespace Saber_Double_Nodes_Serialization
{
    static class Program
    {
        static void Main(string[] args)
        {
            int length = 10;

            ListRand first = new ListRand();
            ListRand second = new ListRand();

            for (int i = 0; i < length; i++)
                first.AddNode();

            first.RandomizeNodes();

            using (FileStream fs = new FileStream("list.txt", FileMode.OpenOrCreate))
            {
                // перезапись существующего файла
                lock (fs)
                {
                    fs.SetLength(0);
                }

                first.Serialize(fs);
            }

            using (FileStream fs = new FileStream("list.txt", FileMode.Open))
            {
                second.Deserialize(fs);
            }

            Console.Read();
        }
    }
}
