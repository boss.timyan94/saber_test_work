﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Saber_Double_Nodes_Serialization
{
    class ListRand
    {
        public int Count;

        public ListNode Head;
        public ListNode Tail;

        #region Serialization
        public void Serialize(FileStream s)
        {
            Stopwatch sw = Stopwatch.StartNew();

            List<ListNode> nodeList = new List<ListNode>();
            ListNode currentNode = Head;

            for (int i = 0; i < Count; i++)
            {
                if (currentNode != null)
                {
                    nodeList.Add(currentNode);
                    currentNode = currentNode.Next;
                }
            }

            sw = Stopwatch.StartNew();

            using (StreamWriter stream = new StreamWriter(s))
            {
                stream.WriteLine(Count);

                for (int i = 0; i < nodeList.Count; i++)
                {
                    stream.WriteLine(nodeList[i].Data.ToString() + ":" + nodeList.IndexOf(nodeList[i].Rand).ToString());
                }
            }

            sw.Stop();
            Console.WriteLine("Serialization Time: {0}", sw.ElapsedMilliseconds);
        }

        public void Deserialize(FileStream s)
        {
            List<ListNode> nodeArray = new List<ListNode>();

            ListNode current = new ListNode();
            Head = current;
            Count = 0;
            string line;

            Stopwatch sw = Stopwatch.StartNew();

            using (StreamReader reader = new StreamReader(s))
            {
                line = reader.ReadLine();
                Count = Convert.ToInt32(line);

                for (int i = 0; i < Count; i++)
                {
                    line = reader.ReadLine();

                    if (line.Equals(""))
                        break;

                    current.Data = line;
                    ListNode next = new ListNode();
                    current.Next = next;
                    nodeArray.Add(current);
                    next.Prev = current;
                    current = next;
                }
            }

            Tail = current.Prev;
            Tail.Next = null;

            for (int i = 0; i < Count; i++)
            {
                string[] data = nodeArray[i].Data.Split(':');

                nodeArray[i].Data = data[0];
                nodeArray[i].Rand = nodeArray[Convert.ToInt32(data[1])];
            }

            sw.Stop();
            Console.WriteLine("Deserialization Time: {0}", sw.ElapsedMilliseconds);
        }

        #endregion

        // Вспомогательное

        #region Node Supports
        public void AddNode()
        {
            Random random = new();

            ListNode newNode = new ListNode()
            {
                Prev = Tail,
                Data = random.Next(0, 1000).ToString()
            };

            if (Head == null)
                Head = newNode;
            else
                Tail.Next = newNode;

            Tail = newNode;
            Count++;
        }

        public void RandomizeNodes()
        {
            Random random = new();
            ListNode current = Head;

            do
            {
                int nodeIndex = random.Next(0, Count);
                ListNode selectedNode = Head;

                for (int i = 0; i < nodeIndex; i++)
                {
                    selectedNode = selectedNode.Next;
                }
                current.Rand = selectedNode;
            }
            while ((current = current.Next) != null);
        }

        #endregion
    }
}
